from helps_rest_create.herlper_create_routes import crear_rutas
from helps_rest_create.herlper_create_serializars import crear_serializers
from helps_rest_create.herlper_create_viewsets import crear_viewsets
from helps_rest_create.list_models import get_lista_modelos


class GeneradorCodigo():
    def __init__(self,app):
        self.app = app
        self.modelos= get_lista_modelos(app)

    def crear_serializer(self):
        print('from ' + self.app + '.models import ', end=' ')
        for modelo in self.modelos:
            print(modelo, end=', ')

        for modelo in self.modelos:
            print(
                '''
                class %sSerializer(serializers.ModelSerializer):
                \tclass Meta:
                \t\tmodel=%s
                \t\tfields='__all__'
                ''' % (modelo, modelo))

    def crear_rutas(self):
        for model in self.modelos:
            path_nom = (model[0]).lower()
            for letra in model[1:]:
                if letra.isupper():
                    path_nom = path_nom + '_' + letra.lower()
                else:
                    path_nom = path_nom + letra

            print('router.register(r\'' + path_nom + '\', views.' + model + 'ViewSet)')

    def crear_admins(self):
        for model in self.modelos:
            print('admin.site.register('+model+')')


    def crear_viewsets(self):
        modelos = self.modelos
        import_string = 'from ' + self.app + '.models import '
        import_serializer = 'from ' + self.app + '.serializers import '
        for modelo in modelos:
            import_string = import_string + modelo + ', '
            import_serializer = import_serializer + modelo + 'Serializer, '

        print(import_string)
        print(import_serializer)

        for modelo in modelos:
            print(
                '''
                class %sViewSet(viewsets.ModelViewSet):
                \tqueryset = %s.objects.all()
                \tserializer_class = %sSerializer
                ''' % (modelo, modelo, modelo))


generar = GeneradorCodigo('seguridad')

#generar.crear_rutas()
generar.crear_admins()