from helps_rest_create.list_models import get_lista_modelos


def crear_serializers(app):
    modelos = get_lista_modelos(app)
    print ('from '+app+'.models import ',end=' ')
    for modelo in modelos:
        print(modelo,end=', ')

    for modelo in modelos:
        print (
    '''
    class %sSerializer(serializers.ModelSerializer):
    \tclass Meta:
    \t\tmodel=%s
    \t\tfields='__all__'
    '''%(modelo, modelo))