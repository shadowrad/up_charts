from helps_rest_create.list_models import get_lista_modelos


def crear_viewsets(app_model):
    modelos = get_lista_modelos(app_model)
    import_string = 'from '+app_model+'.models import '
    import_serializer = 'from '+app_model+'.serializers import '
    for modelo in modelos:
        import_string = import_string+modelo+', '
        import_serializer = import_serializer+ modelo+'Serializer, '

    print(import_string)
    print(import_serializer)

    for modelo in modelos:
        print (
    '''
    class %sViewSet(viewsets.ModelViewSet):
    \tqueryset = %s.objects.all()
    \tserializer_class = %sSerializer
    '''%(modelo, modelo, modelo))

