import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "core.settings")
from django.apps import apps
from django.contrib import admin
from django.contrib.admin.sites import AlreadyRegistered
import django
django.setup()



def get_lista_modelos(app):
    app_models = apps.get_app_config(app).get_models()
    modelos = []
    for model in app_models:
        try:
            modelos.append(model._meta.object_name)
        except AlreadyRegistered:
            pass
    return modelos