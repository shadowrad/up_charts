import os

from helps_rest_create.list_models import get_lista_modelos

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "core.settings")
import django.apps
django.setup()
modelos = django.apps.apps.get_models()



def crear_rutas(app):
    modelos = get_lista_modelos(app)
    for model in modelos:
        path_nom = (model[0]).lower()
        for letra in model[1:]:
            if letra.isupper():
                path_nom = path_nom+'_' +letra.lower()
            else:
                path_nom= path_nom+letra

        print('router.register(r\''+path_nom+'\', views.'+model+'ViewSet)')

