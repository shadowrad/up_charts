from django.contrib import admin

# Register your models here.
from django.forms import ModelForm

from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User, Group

from seguridad.models import Usuario, Perfil, Componente, Panel, ComponentGroup, ProyectoPermiso


class ComponenteInline(admin.TabularInline):
    model = Componente
    extra = 1


class UsuarioInline(admin.TabularInline):
    model = Usuario
    extra = 1


class TemplateAdmin(admin.ModelAdmin):
    inlines = [ComponenteInline]





class UserCreationForm(ModelForm):
    class Meta:
        model = Usuario
        #exclude = ('groups', 'user_permissions',)
        fields = ('email',)


    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user


class UsuarioAdmin(UserAdmin):
    form = UserCreationForm
    fieldsets = (
        (None, {'fields': ('username','email', 'password', 'first_name', 'last_name', 'is_superuser', 'is_staff', 'is_active','perfiles')}),
    )
    list_display = ('username','email', 'first_name', 'last_name')


class PerfilAdmin(admin.ModelAdmin):
    pass


admin.site.register(Usuario, UsuarioAdmin)
admin.site.register(Perfil)
admin.site.register(ComponentGroup)
admin.site.register(Panel)
admin.site.register(Componente)
admin.site.register(ProyectoPermiso)
