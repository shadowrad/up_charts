from django.contrib.auth.models import AbstractUser
from django.db import models

# Create your models here.
from comunicaciones.models import Proyecto

class Usuario(AbstractUser):
    perfiles = models.ManyToManyField('Perfil', blank=True,related_name='usuarios')
    class Meta:
        verbose_name_plural = "Usuarios"

    def __str__(self):
        return self.username


class Perfil(models.Model):
    descripcion = models.CharField(max_length=255)

    class Meta:
        verbose_name_plural = "Perfiles"

    def __str__(self):
        return self.descripcion




class ComponentGroup(models.Model):
    nombre = models.CharField(max_length=5, db_index=True)

    class Meta:
        verbose_name_plural = "Component Groups"

    def __str__(self):
        return self.nombre

class Panel(models.Model):
    nombre = models.CharField(max_length=100)
    component_group = models.ForeignKey(ComponentGroup, related_name='paneles', on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "Paneles"

    def __str__(self):
        return self.nombre


class Componente(models.Model):
    nombre = models.CharField(max_length=100)
    perfiles= models.ManyToManyField(Perfil, related_name='componentes')
    component_group = models.ForeignKey(ComponentGroup, related_name='componentes', on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "Componentes"

    def __str__(self):
        return self.nombre


class ProyectoPermiso(models.Model):
    usuario = models.ForeignKey(Usuario,related_name='proyecto_permisos',on_delete=models.CASCADE)
    proyecto = models.ForeignKey(Proyecto, related_name='proyecto_permisos', on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "Proyecto Permisos"

    def __str__(self):
        return self.proyecto.descripcion
