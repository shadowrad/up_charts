from seguridad.models import Usuario, Panel, Perfil, Componente
from rest_framework import serializers


class UsuarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuario
        fields = ('username','email', 'first_name', 'last_name', 'is_superuser', 'is_staff', 'is_active','perfiles')


class UsuarioLoginSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuario
        fields = ('username','password')


class PanelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Panel
        fields ='__all__'




class PerfilSerializer(serializers.ModelSerializer):
    class Meta:
        model = Perfil
        fields ='__all__'


class ComponenteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Componente
        fields ='__all__'
