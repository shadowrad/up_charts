"""core URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls.static import static
from django.urls import path


from django.conf.urls import url, include
from rest_framework import routers

from core import settings
from seguridad import views as v_seg
from comunicaciones import views
from seguridad.views import login

router = routers.DefaultRouter()
router.register(r'users', v_seg.UserViewSet)
router.register(r'paneles', v_seg.PanelViewSet)
router.register(r'perfil', v_seg.PerfilesViewSet)
router.register(r'componente', v_seg.ComponenteViewSet,base_name='componentes')


router.register(r'proyecto', views.ProyectoViewSet)
router.register(r'tipo_datos', views.TipoDatosViewSet)
router.register(r'tipo_acceso', views.TipoAccesoViewSet)
router.register(r'tipo_dinamica', views.TipoDinamicaViewSet)
router.register(r'base_dato', views.BaseDatoViewSet)
router.register(r'clase', views.ClaseViewSet)
router.register(r'variable_clase', views.VariableClaseViewSet)
router.register(r'parametro', views.ParametroViewSet)
router.register(r'instancia', views.InstanciaViewSet)
router.register(r'conexion', views.ConexionViewSet)
router.register(r'variable_instancia', views.VariableInstanciaViewSet)


urlpatterns = [
    url('', admin.site.urls),
    url(r'^api/', include(router.urls)),
    #url(r'^api/login', v_seg.LoginApi.as_view(),name='login'),
    path('api/login', login)

]+static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
