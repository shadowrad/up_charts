from rest_framework import serializers
from comunicaciones.models import Proyecto, TipoDatos, TipoAcceso, TipoDinamica, BaseDatos, Clase, VariableClase, \
    Parametro, Instancia, Conexion, VariableInstancia


class ProyectoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Proyecto
        fields = '__all__'


class TipoDatosSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoDatos
        fields = '__all__'


class TipoAccesoSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoAcceso
        fields = '__all__'


class TipoDinamicaSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoDinamica
        fields = '__all__'


class BaseDatoSerializer(serializers.ModelSerializer):
    class Meta:
        model = BaseDatos
        fields = '__all__'


class ClaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Clase
        fields = '__all__'


class VariableClaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = VariableClase
        fields = '__all__'


class ParametroSerializer(serializers.ModelSerializer):
    class Meta:
        model = Parametro
        fields = '__all__'


class InstanciaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Instancia
        fields = '__all__'


class ConexionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Conexion
        fields = '__all__'


class VariableInstanciaSerializer(serializers.ModelSerializer):
    class Meta:
        model = VariableInstancia
        fields = '__all__'
