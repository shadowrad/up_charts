from django.contrib import admin

from comunicaciones.models import Proyecto, TipoDatos, Parametro, BaseDatos, TipoAcceso, TipoDinamica, Clase, \
    VariableClase, Instancia, Conexion, VariableInstancia

# Register your models here.
admin.site.register(Proyecto)
admin.site.register(TipoDatos)
admin.site.register(TipoAcceso)
admin.site.register(TipoDinamica)
admin.site.register(BaseDatos)
admin.site.register(Clase)
admin.site.register(VariableClase)
admin.site.register(Parametro)
admin.site.register(Instancia)
admin.site.register(Conexion)
admin.site.register(VariableInstancia)
