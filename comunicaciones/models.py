from django.db import models



class Proyecto(models.Model):
    descripcion = models.CharField(max_length=200)

    class Meta:
        verbose_name_plural = "Proyectos"

    def __str__(self):
        return self.descripcion

#CONFIGURACION
class TipoDatos(models.Model):
    tipo_dato= models.CharField(max_length=200)
    key= models.CharField(max_length=250)
    value = models.TextField()

    class Meta:
        verbose_name_plural = "Tipo de datos"

    def __str__(self):
        return self.tipo_dato

class TipoAcceso(models.Model):
    tipo_acceso= models.CharField(max_length=200)
    key= models.CharField(max_length=250)
    value = models.TextField()

    class Meta:
        verbose_name_plural = "Tipos de acceso"

    def __str__(self):
        return self.tipo_acceso

class TipoDinamica(models.Model):
    descripcion = models.CharField(max_length=250)

    class Meta:
        verbose_name_plural = "Tipos de dinamica"

    def __str__(self):
        return self.descripcion

class BaseDatos(models.Model):
    descripcion = models.CharField(max_length=250)

    class Meta:
        verbose_name_plural = "Bases de datos"

    def __str__(self):
        return self.descripcion

#DEFINICION CLASES
class Clase(models.Model):
    nombre = models.CharField(max_length=200)
    descripcion = models.CharField(max_length=250)
    padre = models.ForeignKey('self',on_delete=models.CASCADE,related_name="hijos")
    tipo_acceso = models.ForeignKey(TipoAcceso,on_delete=models.CASCADE)
    tipo_dinamica = models.ForeignKey(TipoDinamica, on_delete=models.CASCADE)
    base_datos = models.ForeignKey(BaseDatos, on_delete=models.CASCADE, related_name="tipos_datos")

    class Meta:
        verbose_name_plural = "Clases"

    def __str__(self):
        return self.descripcion

class VariableClase(models.Model):
    clase = models.ForeignKey(Clase, on_delete=models.CASCADE, related_name='variables_clase')
    nombre = models.CharField(max_length=200)
    tipo_dato= models.ForeignKey(TipoDatos, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "Variables Clases"

    def __str__(self):
        return self.nombre

class Parametro(models.Model):
    variable_clase = models.ForeignKey(VariableClase, on_delete=models.CASCADE, related_name='paramtros')
    key = models.CharField(max_length=200)
    value = models.TextField()

    class Meta:
        verbose_name_plural = "Parametros"

    def __str__(self):
        return self.key

#PARAMETRIZACION POR INSTANCIA
class Instancia(models.Model):
    clase = models.ForeignKey(Clase, related_name='instancias', on_delete=models.CASCADE)
    proyecto = models.ForeignKey(Proyecto, related_name='instancias', on_delete=models.CASCADE)
    address = models.TextField()
    frecuencia = models.IntegerField()
    last_start_read = models.DateTimeField(auto_now=True)
    last_end_read = models.DateTimeField(default=0)
    descripcion = models.TextField()

    class Meta:
        verbose_name_plural = "Instancias"

    def __str__(self):
        return self.descripcion

class Conexion(models.Model):
    instancia = models.ForeignKey(Instancia, related_name='conexiones', on_delete=models.CASCADE)
    clase = models.ForeignKey(Clase, related_name='conexiones',on_delete=models.CASCADE)
    key = models.CharField(max_length=200)
    value = models.TextField()

    class Meta:
        verbose_name_plural = "Conexiones"

    def __str__(self):
        return self.key

class VariableInstancia(models.Model):
    variable_clase= models.ForeignKey(VariableClase,related_name='variables_instancias', on_delete=models.CASCADE)
    instancia= models.ForeignKey(Instancia, related_name='variables_instancias', on_delete=models.CASCADE)
    key = models.CharField(max_length=200)
    value = models.TextField()

    class Meta:
        verbose_name_plural = "Variables instancia"

    def __str__(self):
        return self.key