from django.shortcuts import render
from rest_framework import viewsets

from comunicaciones.models import Proyecto, TipoDatos, TipoAcceso, TipoDinamica, BaseDatos, Clase, VariableClase,\
    Parametro, Instancia, Conexion, VariableInstancia
from comunicaciones.serializers import ProyectoSerializer, TipoDatosSerializer, TipoAccesoSerializer,\
    TipoDinamicaSerializer, BaseDatoSerializer, ClaseSerializer, VariableClaseSerializer, ParametroSerializer,\
    InstanciaSerializer, ConexionSerializer, VariableInstanciaSerializer


class ProyectoViewSet(viewsets.ModelViewSet):
    queryset = Proyecto.objects.all()
    serializer_class = ProyectoSerializer


class TipoDatosViewSet(viewsets.ModelViewSet):
    queryset = TipoDatos.objects.all()
    serializer_class = TipoDatosSerializer


class TipoAccesoViewSet(viewsets.ModelViewSet):
    queryset = TipoAcceso.objects.all()
    serializer_class = TipoAccesoSerializer


class TipoDinamicaViewSet(viewsets.ModelViewSet):
    queryset = TipoDinamica.objects.all()
    serializer_class = TipoDinamicaSerializer


class BaseDatoViewSet(viewsets.ModelViewSet):
    queryset = BaseDatos.objects.all()
    serializer_class = BaseDatoSerializer


class ClaseViewSet(viewsets.ModelViewSet):
    queryset = Clase.objects.all()
    serializer_class = ClaseSerializer


class VariableClaseViewSet(viewsets.ModelViewSet):
    queryset = VariableClase.objects.all()
    serializer_class = VariableClaseSerializer


class ParametroViewSet(viewsets.ModelViewSet):
    queryset = Parametro.objects.all()
    serializer_class = ParametroSerializer


class InstanciaViewSet(viewsets.ModelViewSet):
    queryset = Instancia.objects.all()
    serializer_class = InstanciaSerializer


class ConexionViewSet(viewsets.ModelViewSet):
    queryset = Conexion.objects.all()
    serializer_class = ConexionSerializer


class VariableInstanciaViewSet(viewsets.ModelViewSet):
    queryset = VariableInstancia.objects.all()
    serializer_class = VariableInstanciaSerializer